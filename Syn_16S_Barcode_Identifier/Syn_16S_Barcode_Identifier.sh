#!/bin/sh

# NOTE 21Nov2012
# The probes for SynCya1 and the second set of probes for SynI are identical
# usage: sh probe_analysis_annotated4.sh file.fasta
# The sequence tags and filenames are hardcoded and need to be edited in line93 and line103 respectively.

echo "This script looks for sequence patterns characteristic for different cyanobacteria clades."
echo "See source code for probe sequences and clades detected. Version 26Oct2012"
echo
if [ -z $1 ];then 
	echo "ERROR: 16S fasta file not specified"
	exit
fi

################### grepfest ##############################################
ext='detected.fas'

grep 'CCATATGCCGATAGGTGAAATGAATTTCGCCTGAGGATGAGCT' -B1 $1 > ProHLI.$ext
grep 'CCATATGCCGATAGGTGAAATGAATCTCGCCTGAGGATGAGCT' -B1 $1 >> ProHLI.$ext
grep 'CCATATGCCGATAGGCGAAATGAATTTCGCCTGAGGATGAGCT' -B1 $1 >> ProHLI.$ext
grep 'CCATATACCGATAGGTGAAATGAATTTCGCCTGAGGATGAGCT' -B1 $1 >> ProHLI.$ext
grep 'CCATATGCCGACAGGTGAAATGAATTTCGCCTGAGGATGAGCT' -B1 $1 >> ProHLI.$ext

grep 'CCATATGCCTACTGGTGAAATGAATTTCGCCTGAGGATGAGCT' -B1 $1 > ProHLII.$ext
grep 'CCATATGCCTACTGGTGAAACGAATTTCGCCTGAGGATGAGCT' -B1 $1 >> ProHLII.$ext
grep 'CCATATGCCTATTGGTGAAATGAATTTCGCCTGAGGATGAGCT' -B1 $1 >> ProHLII.$ext

grep 'TAGGTGAAATTAATTTCGCCTGAGGATGAGCCCGCGTCTGATTAGCTAGTTGGTA' -B1 $1 > ProHNLC2.$ext
grep 'TAGGTGAAATCAATTTCGCCTGAGGATGAGCCCGCGTCTGATTAGCTAGTTGGTA' -B1 $1 >> ProHNLC2.$ext

grep 'AAGGTGAAATTAATTTCGCCTGAGGATGAGCCCGCGTCTGATTAGCTAGTTGGTA' -B1 $1 > ProHNLC1.$ext
grep 'AAGGTGAAATCAATTTCGCCTGAGGATGAGCCCGCGTCTGATTAGCTAGTTGGTA' -B1 $1 >> ProHNLC1.$ext

grep 'AGGATGAGCCCGCGTCTGATTAGCTTGTTGGTGAGGTAATGGCTCACCAAGGCA' -B1 $1 > ProLLI.$ext

grep 'AGGATGAGCCCGCGTCTGATTAGCTTGTTGGTGAGGTAATGGCTCACCAAGGCT' -B1 $1 > ProLLII.$ext

grep 'GAAAGGTGAAATGAATTTCGCCTGAGGATGAGCCCGCGTCTGATTAGCTAGTTGGTG' -B1 $1 > ProLLIII.$ext

grep 'ATTTCGCCTGAGGATGAGCCCGCGTCTGATTAGCTAGTTGGTGAGGTAATGGCTCACCAAGGCAT' -B1 $1 > ProLLIV.$ext

grep 'AGGATGAGCCCGCGTCTGATTAGCTAGTTGGTGAGGTAAG' -B1 $1 | grep 'TAACGGCTGGAAACGGCC' -B1 > SynV_VI_VII.$ext

grep 'AGGATGAGCCCGCGTCTGATTAGCTAGTTGGTGAGGTAAG' -B1 $1 | grep 'TAACGGTTGGAAACGACC' -B1 > SynMBARI_A.$ext

grep 'AGGATGAGCCCGCGTCTGATTAGCTAGTTGGTGAGGTAAG' -B1 $1 | grep 'TAACGGTTGGAAACGGCC' -B1 > SynI.$ext
grep 'AGGATGAGCCCGCGTCTGATTAGCTAGTTGGTGAGGTAAG' -B1 $1 | grep 'TAACGGCCGGAAACGGCC' -B1 >> SynI.$ext

grep 'AGGATGAGCTCGCGTCTGATTAGCTAGTTGGTGG' -B1 $1 | grep 'AGAATCTGCCCTCAGGAGGGGGATAACGGTT' -B1 > SynIII.$ext

grep 'AGTTGGTGTAGGTAATGGCTCACCAAGGCA' -B1 $1 | grep 'TAACGGTTGGAAACGACCGCTAATACCCC' -B1 > SynWPC2.$ext

grep 'AGTTGGTGTAGGTAATGGCTCACCAAGGCA' -B1 $1 | grep 'TAACGGCTGGAAACGGCCGCTAATACCCC' -B1 > SynMBARI_B.$ext

grep 'AGTTGGTGTAGGTAATGGCTCACCAAGGCA' -B1 $1 | grep 'CAACAGCTGGAAACGGCTGCTAATACCCC' -B1 > SynII.$ext

grep 'AGTTGGTGTAGGTAATGGCTCACCAAGGCA' -B1 $1 | grep 'TAACAGCTGGAAACGGCTGCTAATACCCC' -B1 > SynIV.$ext

grep 'AGGATGAGCTCGCGTCTGATTAGCTAGTTGGTGTAGGTAAAGGCTCACCAAGGCA' -B1 $1 > SynWPC1.$ext

grep 'AGGATGAGCTCGCGTCTGATTAGCTTGTTGGTGGGGTAATGGCCTACCAAGGCA' -B1 $1 > SynX.$ext

grep 'AGGATGAGCTCGCGTCTGATTAGCTAGTTGGTGAGGTAATGGCTCACCAAGGCA' -B1 $1 > SynIX.$ext

grep 'AGGATGAGCTCGCGTCTGATTAGCTAGTTGGTGG' -B1 $1 | grep 'AGAATCTGCCCTCAGGAGGGGGACAACAGCT' -B1 > SynVIII.$ext

grep 'GGGATGAGCCCGCGTCTGATTAGCTAGTTGGTGAGGTAATGGCTCACCAAGGC' -B1 $1 > SynSubII.$ext

grep 'GGAATCTGCCCTCAGGAGGGGGATAACGGCC' -B1 $1 | grep 'AGGATGAGCCCGCGTCTGATTAGCTAGTTGGTG' -B1 > SynCya1.$ext

grep 'GTTTTCGCCTGAGGATGAGCCCGCGTCTGATTAGCTAGTTGGTGT' -B1 $1 > SynAF448065.$ext

grep 'TCGCGTCTGATTAGCTAGTTGGTGG' -B1 $1 | grep 'AGAATCTGCCCCAAGGAGGGGGATAACGGCT' -B1 > SynCya2.$ext

grep 'TGAGGATGAGCTCGCGTCTGATTAGCTAGTTGGTGTGGTAAAGGCTCACCAA' -B1 $1 > SynSubI.$ext

grep 'TGAGGATGAGCTCGCGTCTGATTAGCTAGTTGGTAGGGTAAAGGCCTACCAA' -B1 $1 > SynH.$ext

sed -e 's/-//g' -i *.$ext #removes -- introduced by grep
sed '/^$/d' -i *.$ext #removes empty lines

echo "Assigned sequences saved in *.detected.fas"

############################### summary output to the screen ###############################
grep '^>' -c *.$ext
echo -n "Total number of assigned sequences:"
cat *.$ext | grep '^>' -c
echo -n "Total number of sequences in source file:"
grep '^>' -c $1

############################### tabulate results into csv file ##########################
tag1='>AACCAACC' #sequence tags (corresponds to samples)
tag2='>AACGAACG'
tag3='>AACGTTCG'
tag4='>AAGCTACC'
tag5='>AAGGTACG'
tag6='>AACGCGAA'
tag7='>AACCGGAA'
tag8='>AATAGCGG'

echo -n "run on: " > probe_assignment_$1.csv; date | tr -d '\012\015' >> probe_assignment_$1.csv #tr removes the line break output
echo ",H3N10,70N10,70N6,155N10,155N5,CN207_155surf08a,CN207_155surf08b,CN207_155surf30" >> probe_assignment_$1.csv # names for samples (corresponds to sequence tags)
echo "," $tag1 "," $tag2 "," $tag3 "," $tag4 "," $tag5 "," $tag6 "," $tag7 "," $tag8 >> probe_assignment_$1.csv
for file in $(ls *.detected.fas); do
	echo -n $file >> probe_assignment_$1.csv; 
	for j in $tag1 $tag2 $tag3 $tag4 $tag5 $tag6 $tag7 $tag8; do
		echo -n ',' >> probe_assignment_$1.csv
		grep -c $j $file | tr -d '\012\015' >> probe_assignment_$1.csv 
		done
	echo >> probe_assignment_$1.csv; done
echo
echo "Table saved as probe_assignment_$1.csv"

