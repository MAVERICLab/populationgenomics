#!/usr/bin/perl
use warnings;
use strict;
use Bio::SeqIO;

my @introns;
my $p_introns = $ARGV[0];  # list of identified potential "paired" genes with introns
open(INTRONS, $p_introns) or die "Could not open $p_introns \n";
while (my $line = <INTRONS>) {
	chomp $line;
	my @spl = split(/\t/, $line);
	push(@introns, $line);
	
}
close INTRONS;


my @files = glob("*.fa.algn"); #change to suffix that you used for your alignments
foreach my $fil (@files) {
	$fil =~ /^(.+).fa.algn/; #change to suffix that you used for your alignments
	my $prefix = $1;
	my %introns_pairs;
	my %id_seqs;
	my %ids_introns;
	foreach my $i (@introns) {
		my @spl = split(/\t/, $i);
		$spl[1]=~ /^(\S+)_(\d+)/;
		my $number1 = $2;
		$spl[2]=~ /^(\S+)_(\d+)/;
		my $number2 = $2;
		my $id1;
		my $id2;
		if ($number1 < $number2) {
			$id1 = $spl[1];
			$id2 = $spl[2];
		}
		else {
			$id1 = $spl[2];
			$id2 = $spl[1];
		}
		if ($spl[0] eq $prefix) {
			$introns_pairs{$id1}=$id2;
		}
		$ids_introns{$id1}=$id1;
		$ids_introns{$id2}=$id2;
	}
	my $outfile = $prefix.".no_introns.fa.algn";
	open(OUTFILE, '>', $outfile) or die "Could not open $outfile \n";
	my $seqio  = Bio::SeqIO->new(-format => 'fasta', -file   => $fil);
	while (my $seqobj = $seqio->next_seq) {
		my $seqid = $seqobj->display_id;
		my $nuc = $seqobj->seq();
		$id_seqs{$seqid}=$nuc;
	}
	foreach my $id (keys %id_seqs) {
		if (exists $ids_introns{$id}) {
		}
		else {
			print OUTFILE ">".$id."\n".$id_seqs{$id}."\n";
		}
	}
	foreach my $i_1 (keys %introns_pairs) {
		$i_1 =~ /^(\S+)_(\d+)/;
		my $genome1 = $1;
		my $number1 = $2;
		my $number2;
		my $seq1 = $id_seqs{$i_1};
		my $seq_length = length($seq1);
		my $last_block = ($seq_length-40);
		my $cut_mark;
		my $gene_start;
		my $gene_end;
		foreach my $block_number1 (0 ... $last_block) {
			my $nucleotide_block1 = substr($seq1, $block_number1, 40
);
			if ($nucleotide_block1 eq "-----------------------------
-----------") {
				$cut_mark = $block_number1;
				last; 
			}
		}
		$gene_start = substr($seq1,0,$cut_mark);
		my $end_length = ($seq_length-$cut_mark);
		foreach my $i_2 (values %introns_pairs) {
			$i_2 =~ /^(\S+)_(\d+)/;
			my $genome2 = $1;
			$number2 = $2;
			if ($genome1 eq $genome2) {
				my $seq2 = $id_seqs{$i_2};
				$gene_end = substr($seq2, $cut_mark, $end_length
);
				last;
			}
		}
		print OUTFILE ">".$genome1."_".$number1."_".$number2." intron_re
moved "."\n".$gene_start.$gene_end."\n";	
	}
}