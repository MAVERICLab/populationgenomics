#!/usr/bin/perl
use warnings;
use strict;
use Bio::SeqIO;

my $outfile = "Unique_Polymorphisms.txt";
open(OUTFILE, '>', $outfile) or die "Could not open $outfile \n";

my @core_genomic_files = glob("*.fasta");
foreach my $fasta (@core_genomic_files) {
	$fasta =~ /^(.+).fasta/;
	my $cluster = $1;
	my $seqio = Bio::SeqIO->new(-format => 'fasta', -file   => $fasta);
	my @coastal_phages;
	my @upwelling_phages;
	my $coastal_count = 0;
	my $upwelling_count = 0;
	while (my $seqobj = $seqio->next_seq) {
		my $seqid = $seqobj->display_id;
		my $nuc = $seqobj->seq();
		my @spl = split(/\./, $seqid);
		if ($spl[0] =~ /^Syn7803C[0-9]/ || $spl[0] =~ /^S_MbCM[0-9]/) {
			$coastal_count++;
			push(@coastal_phages, [$spl[0],$nuc]);
		}
		if ($spl[0] =~ /^Syn7803US[0-9]/) {
			$upwelling_count++;
			push(@upwelling_phages, [$spl[0],$nuc]);
		}	
	}
	my $consensus = $coastal_phages[0][1];
	my @nucleotides = split(//, $consensus);
	my $unique_fixed_count = 0;
	foreach my $i (0 ... $#nucleotides) {
		my $nuc_consensus = $nucleotides[$i];
		my $c_internal_count =0;
		my $us_internal_count =0;
		foreach my $j (0 ... $#coastal_phages) {
			my $c_genome_nuc = $coastal_phages[$j][1];
			my @c_nuc_reference = split(//, $c_genome_nuc);
			if ($nuc_consensus eq $c_nuc_reference[$i]) {
				$c_internal_count++;
			}
		}
		foreach my $k (0 ... $#upwelling_phages) {
			my $us_genome_nuc = $upwelling_phages[$k][1];
			my @us_nuc_reference = split(//, $us_genome_nuc);
			if ($nuc_consensus ne $us_nuc_reference[$i]) {
				$us_internal_count++;
			}
		}
		if (($c_internal_count == $coastal_count && $us_internal_count == 0) || ($us_internal_count == $upwelling_count && $c_internal_count == 0)) {
			$unique_fixed_count++;
		}
	}
	print OUTFILE $cluster."\t".$unique_fixed_count."\n";
}

close OUTFILE;