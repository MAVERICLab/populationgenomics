#!/usr/bin/perl
use warnings;
use strict;
use Bio::PopGen::Statistics;


my $color1 = $ARGV[0]; #note color1 should be the cross comparison files
my $color2 = $ARGV[1];
my $color3 = $ARGV[2];

my %hash1;
my %hash2;
my %hash3;

my $file1 = $color1."_syn_nonsyn.txt"; #polymorphic values;
open(FILE1, $file1) or die "Could not open $file1 \n";
while (my $line1 = <FILE1>) {
	chomp $line1;
	my @spl1 = split(/\t/, $line1);
	my @spl1_2 = split(/_/,$spl1[0]);
	my $pc1 = $spl1_2[0]."_".$spl1_2[1]."_".$spl1_2[2];
	my $syn1 = $spl1[1];
	my $nonsyn1 = $spl1[2];
	my $integers1 = $syn1."\t".$nonsyn1;
	$hash1{$pc1} = $integers1;
}
close FILE1;

my $file2 = $color2."_syn_nonsyn.txt";
open(FILE2, $file2) or die "Could not open $file2 \n";
while (my $line2 = <FILE2>) {
	chomp $line2;
	my @spl2 = split(/\t/, $line2);
	my @spl1_2 = split(/_/,$spl2[0]);
        my $pc2 = $spl1_2[0]."_".$spl1_2[1]."_".$spl1_2[2];
	my $syn2 = $spl2[1];
	my $nonsyn2 = $spl2[2];
	my $integers2 = $syn2."\t".$nonsyn2;
	$hash2{$pc2} = $integers2;
}
close FILE2;

my $file3 = $color3."_syn_nonsyn.txt";
open(FILE3, $file3) or die "Could not open $file3 \n";
while (my $line3 = <FILE3>) {
	chomp $line3;
	my @spl3 = split(/\t/, $line3);
	my @spl1_2 = split(/_/,$spl3[0]);
        my $pc3 = $spl1_2[0]."_".$spl1_2[1]."_".$spl1_2[2];
	my $syn3 = $spl3[1];
	my $nonsyn3 = $spl3[2];
	my $integers3 = $syn3."\t".$nonsyn3;
	$hash3{$pc3} = $integers3;
}
close FILE3;

my $outfile1 = $color2."_w_$color1.txt";
open(OUTFILE1,'>', $outfile1) or die "Could not open $outfile1 \n";
foreach my $key1 (sort keys %hash2) {
	if (exists $hash1{$key1}) {
		my @split1 = split(/\t/, $hash2{$key1});
		my @split2 = split(/\t/, $hash1{$key1});
		my $Non_poly1 = $split2[1];
		my $Non_fix1 = $split1[1];;
		my $Syn_poly1 = $split2[0];
		my $Syn_fix1 = $split1[0];
		print OUTFILE1 $key1."\t".$Non_poly1."\t".$Non_fix1."\t".$Syn_poly1."\t".$Syn_fix1."\n";
	}
}
close OUTFILE1;

my $outfile2 = $color3."_w_$color1.txt";
open(OUTFILE2,'>', $outfile2) or die "Could not open $outfile2 \n";
foreach my $key2 (sort keys %hash3) {
	if (exists $hash1{$key2}) {
		my @split1 = split(/\t/, $hash3{$key2});
		my @split2 = split(/\t/, $hash1{$key2});
		my $Non_poly2 = $split2[1];
		my $Non_fix2 = $split1[1];;
		my $Syn_poly2 = $split2[0];
		my $Syn_fix2 = $split1[0];
		print OUTFILE2 $key2."\t".$Non_poly2."\t".$Non_fix2."\t".$Syn_poly2."\t".$Syn_fix2."\n";
	}
}
close OUTFILE2;


my $final_outfile = $color2."_n_".$color3.".MK.txt";
open(FINAL, '>', $final_outfile) or die "Could not open $final_outfile \n";

my $stats_file1 = $color2."_w_$color1.txt";
open(STATS1, $stats_file1) or die "Could not open $stats_file1 \n";
while (my $row = <STATS1>) {
	chomp $row;
	my @break = split(/\t/, $row);
	my $pc = $break[0];
	my $Non_poly = $break[1];
	my $Non_fix = $break[2];
	my $Syn_poly = $break[3];
	my $Syn_fix = $break[4];
	my @stats1;
	for (@break[1..$#break]) {
		push (@stats1, $_);
	}
	my $MK = Bio::PopGen::Statistics-> mcdonald_kreitman_counts(@stats1);
	print FINAL $color2."\t".$pc."\t".$MK."\t";
	if ($MK >= 0.05) {
		print FINAL "Neutral\n";
	}
	else {
		my $percentage_fixed = $Non_fix/($Non_fix+$Syn_fix);
		my $percentage_polymorphic = $Non_poly/($Non_poly+$Syn_poly);
		if ($percentage_fixed > $percentage_polymorphic) {
			print FINAL "Positive Selection \n";
		}
		if ($percentage_polymorphic > $percentage_fixed) {
			print FINAL "Purifying Selection or Heterozygote Advantage \n";
		}
	}
}

my $stats_file2 = $color3."_w_$color1.txt";
open(STATS2, $stats_file2) or die "Could not open $stats_file2 \n";
while (my $row = <STATS2>) {
	chomp $row;
	my @break = split(/\t/, $row);
	my $pc = $break[0];
	my $Non_poly = $break[1];
	my $Non_fix = $break[2];
	my $Syn_poly = $break[3];
	my $Syn_fix = $break[4];
	my @stats2;
	for (@break[1..$#break]) {
		push (@stats2, $_);
	}
	my $MK = Bio::PopGen::Statistics-> mcdonald_kreitman_counts(@stats2);
	print FINAL $color3."\t".$pc."\t".$MK."\t";
	if ($MK >= 0.05) {
		print FINAL "Neutral\n";
	}
	else {
		my $percentage_fixed = $Non_fix/($Non_fix+$Syn_fix);
		my $percentage_polymorphic = $Non_poly/($Non_poly+$Syn_poly);
		if ($percentage_fixed > $percentage_polymorphic) {
			print FINAL "Positive Selection \n";
		}
		if ($percentage_polymorphic > $percentage_fixed) {
			print FINAL "Purifying Selection or Heterozygote Advantage \n";
		}
	}
}

close FINAL;